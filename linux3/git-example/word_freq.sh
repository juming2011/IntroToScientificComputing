#!/bin/bash
# Generate an ordered list of words according to frequency in a text file.

tr -d [:punct:] < $1 | tr [:space:] "\n" | sort | uniq -c | sort -n
