#!/usr/bin/env python3

# This python module is used for generating random numbers.
import random

# Note we add the additonal argmument "w" when we want to write to a file.
f = open("100randoms.dat", "w")

for ii in range(100):
    # random.gauss picks a random number using a gaussian distribution
    # centred at the first argument and a width given by the second argument.
    n = str(random.gauss(100.0, 10.0))
    f.write(str(n) + '\n')
    # Alernatively we could use the 'file=f' option to print as follows
    # print(n, file=f)

f.close()
